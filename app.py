import hashlib as hl
import os
import json

PATH = os.curdir + '/blocks/'


def check_chain(blocks):
    check = {} 
    for block in blocks:
        prev_file = block - 1
        
        with open(PATH + str(block), 'r', encoding="utf-8") as f:
            cur_hash = json.load(f)['hash']
            if cur_hash == get_hash(prev_file):
                check[str(block)] = 'ok!'
            else:
                check[str(block - 1)] = 'corrupted!'
    
    return check

def get_files():
    dirlist = os.listdir(PATH)
    
    return sorted([int(i) for i in dirlist])

def get_hash(last_file):
    with open(PATH + str(last_file), 'r', encoding='utf-8') as f:
        hash = hl.md5(f.read().encode('utf-8'))
        return hash.hexdigest()

def create_block(name, sum, to, hash, filename):
    data = {
        "name": name,
        "count": sum,
        "to": to,
        "hash": hash, 
    }
    
    with open(PATH + str(filename), 'w') as f:
        json.dump(data, f,indent=4, ensure_ascii=False)
        

def main(flag, **kwargs):
    
    files = get_files()
    last_file = files[-1]
    hash = get_hash(last_file)
    
    if flag:
       create_block(kwargs['name'], kwargs['to'], kwargs['sum'], hash, last_file + 1)
    else:
        return check_chain(files[1:])
    
    
if __name__ == "__main__":
    main()