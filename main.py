from flask import Flask, render_template, request
import app

web = Flask(__name__)

@web.route('/', methods=['post', 'get'])
def index():
    
    if request.method == 'POST':
        firstname = request.form.get('name')
        sum = request.form.get('sum')
        to = request.form.get('to')
        app.main(True, name=firstname, sum=sum, to=to)
        
    
    return render_template('index.html')

@web.route('/check')
def check():
    
    blocks = app.main(False)
    return render_template('check.html', blocks=blocks)

if __name__ == '__main__':
    web.run(debug=True)